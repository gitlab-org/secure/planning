# Secure Team Planning

![Build Status](https://gitlab.com/gl-secure/planning/badges/master/build.svg)

GitLab Page for the Secure Team planning.
The file `Security Products.merlin2` must be opened with [Merlin2](https://www.projectwizards.net).

More information on the Secure Team: https://about.gitlab.com/handbook/engineering/ops/secure/
